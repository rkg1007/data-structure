import java.util.HashSet;
import java.util.Scanner;

public class DoubleInversions {
  public static boolean isValid(HashSet<Integer> numUsed, int n, int pos) {
    if (pos > 0 && pos <= n) {
      return !numUsed.contains(pos);
    }
    return false;
  }

  public static int[] findOriginalArray(int[] arrInversions, int[] revInversions) {
    int n = arrInversions.length;
    int[] res = new int[n];
    HashSet<Integer> numUsed = new HashSet<>();
    for (int i = 0; i < n; i++) {
      int pos = n - arrInversions[i] - revInversions[n - i - 1];
      if (isValid(numUsed, n, pos)) {
        numUsed.add(pos);
        res[i] = pos;
      } else {
        res[0] = -1;
        break;
      }
    }
    return res;
  }
  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int tc = in.nextInt();
    while (tc-- > 0) {
      int n = in.nextInt();
      int[] arrInversions = new int[n];
      int[] revInversions = new int[n];
      for (int i = 0; i < n; i++) {
        arrInversions[i] = in.nextInt();
      }
      for (int i = 0; i < n; i++) {
        revInversions[i] = in.nextInt();
      }
      int[] originalArray = findOriginalArray(arrInversions, revInversions);
      if (originalArray[0] == -1) {
        System.out.print(-1);
      } else {
        for (int num : originalArray) {
          System.out.print(num + " ");
        }
      }
      System.out.println();
    }
    in.close();
  }
}